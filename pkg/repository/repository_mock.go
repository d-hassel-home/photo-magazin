package repository

import (
	"fmt"
	"os"
)

type MockRepo struct {
	AddIndex func(details os.FileInfo)
}

func (r *MockRepo) addIndex(details os.FileInfo) {
	fmt.Println("test")
}
